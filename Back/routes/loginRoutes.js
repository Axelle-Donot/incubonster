const express = require("express")
const router = express.Router()
const loginController = require("../controllers/loginController")

router.post("/login", loginController.connect)

// Autres routes liées aux produits

module.exports = router
