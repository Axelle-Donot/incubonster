const express = require("express")
const verifyToken = require("../middleware");
const helloController = require("../controllers/helloController");
const router = express.Router()

router.post("/hello", helloController.getHello)

module.exports = router