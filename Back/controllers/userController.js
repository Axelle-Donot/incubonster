const g_userDB = require("../../microService/dbConfig").g_userDB // Utiliser le fichier dbConfig.js

exports.getUserById = (req, res) => {
  const userId = req.body.id // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  g_userDB.query(
    "SELECT * FROM user WHERE user.id = ?",
    [userId],
    (err, results) => {
      if (err) {
        console.error("Erreur lors de la récupération des produits :", err)
        res.status(500).json({ error: "Erreur interne du serveur" })
        return
      }
      res.json(results)
    }
  )
}
