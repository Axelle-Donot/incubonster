const { g_cartDB, g_userDB } = require("../../microService/dbConfig");


exports.getCart = (req, res) => {
  const { email } = req.body // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  g_userDB.query("SELECT id FROM user WHERE user.email = ?",
    [email],
    (err, results) => {
      if (err) {
        console.error("Erreur lors de la récupération des infos user", err);
        callback({ error: "Erreur interne du serveur" });
        return;
      }
      //------------------------Recherche si le produit est dj dans le cart du user---------------------------
      userId = results[0].id
      g_cartDB.query(`SELECT cp.id_cart, cp.id_product, cp.quantity
      FROM cartproduct AS cp
      JOIN cart AS c ON cp.id_cart = c.id
      WHERE c.id_client = ?;
      `,
        [userId],
        (err, results) => {
          if (err) {
            console.error("Erreur lors de la récupération des infos user", err);
            res.json({ error: "Erreur interne du serveur" });
            return;
          }
          if (results.length > 0) {
            res.json(results)
          } else {
            res.json(null)
          }

        }
      )
    });
}
