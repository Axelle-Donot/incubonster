const connection = require("../../microService/dbConfig").connection; // Utiliser le fichier dbConfig.js

const jwt = require("jsonwebtoken");
const secretKey = "BuggatiBolide"; // Remplacez cela par votre propre clé secrète pour la signature JWT

// Fonction pour gérer l'authentification et générer le token
exports.connect = (req, res) => {
  const { email, password } = req.body;

  connection.query(
    "SELECT * FROM User WHERE user.email = ?",
    [email],
    (err, results) => {
      if (err) {
        console.error("Erreur lors de la récupération des produits :", err);
        res.status(500).json({ error: "Erreur interne du serveur" });
        return;
      }
      if (results.length !== 1) {
        console.error("L'utilisateur n'existe pas");
        res.status(404).json({ error: "Username doesn't existe" });
        return;
      }
      if (results[0].password !== password) {
        console.error("Bad Password");
        res.status(403).json({ error: "Bad Password" });
        return;
      }

      const user = results[0];

      // Générer le token JWT
      jwt.sign({ user }, secretKey, { expiresIn: "1h" }, (err, token) => {
        if (err) {
          res
            .status(500)
            .json({ error: "Erreur lors de la génération du token" });
        } else {
          res.json({ token });
        }
      });
    }
  );
};
