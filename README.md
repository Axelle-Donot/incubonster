BOUHENAF Irfan
DONOT Axelle
CORSO Gaetan

INFRES 15

# Bienvenue sur notre application

Bonjour ! Pour lancer l'application, suivez ces étapes simples.

## 🚀 Démarrage rapide

### Prérequis

- Avoir **Docker** et **Docker Compose** installés sur votre machine. Si ce n'est pas le cas, consultez le guide d'installation sur le [site officiel de Docker](https://docs.docker.com/get-docker/).

### Lancer l'application

1. Ouvrez un terminal et naviguez jusqu'à la racine du projet.

2. Exécutez la commande suivante :
```shell
docker-compose up
```

Pour démarrer correctement l'application, il faut lancer le docker 2 fois.
Une fois les conteneurs démarrés, l'application sera accessible via l'URL suivante : http://localhost:3000
Cependant, l'application n'a pas pu être finalisé à temps.

## 🔑 Connexion à l'application

Pour vous connecter à l'application, veuillez utiliser les informations suivantes :

    Pseudo : sacha

## 📚 Tester l'API

Pour tester l'API indépendamment du front end, il suffit d'utiliser un outil comme Postman et tester l'API indépendamment.
Vous pouvez vous référer à la documentation de l'API : https://app.swaggerhub.com/apis/DONOTAXELLE/Incubonster/1.0.0