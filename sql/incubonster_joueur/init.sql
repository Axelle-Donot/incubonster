CREATE DATABASE IF NOT EXISTS incubonster_joueur
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

USE incubonster_joueur;

CREATE TABLE joueur (
    id INT AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(64) NOT NULL,
    money INT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE monster (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(64),
    pv INT,
    attaque INT,
    defense INT,
    vitesse INT,
    number INT,
    joueur_id INT,
    type VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE oeuf (
    id INT AUTO_INCREMENT PRIMARY KEY,
    joueur_id INT,
    timeOfIncubation DATETIME
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

USE incubonster_joueur;

INSERT INTO joueur (pseudo, money)
VALUES ('sacha', 0);
