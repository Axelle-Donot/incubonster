CREATE DATABASE IF NOT EXISTS incubonster_incubateur
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

USE incubonster_incubateur;

CREATE TABLE oeuf (
    id INT AUTO_INCREMENT PRIMARY KEY,
    joueur_id INT,
    timeOfIncubation DATETIME
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE incubateur (
    id INT AUTO_INCREMENT PRIMARY KEY,
    joueur_id INT,
    number INT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
