CREATE DATABASE IF NOT EXISTS incubonster_monstre
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

USE incubonster_monstre;

CREATE TABLE monstre (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(64),
    type VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Ajouter des monstres dans la table "monstre" de la base de données "incubonster_monstre" avec des noms originaux
USE incubonster_monstre;
INSERT INTO monstre (nom, type) VALUES
    ('Flareonix', 'feu'),
    ('Aqualith', 'eau'),
    ('Folialon', 'plante'),
    ('Terragolem', 'sol'),
    ('Rockspire', 'roche'),
    ('Frostbite', 'glace'),
    ('Steelixor', 'acier'),
    ('Combatron', 'combat'),
    ('Normaleon', 'normal'),
    ('Spectrifyre', 'spectre'),
    ('Psyquill', 'psy'),
    ('Shadowclaw', 'ténèbres'),
    ('Faerywhisp', 'fée'),
    ('Dragovolt', 'dragon'),
    ('Venomfang', 'poison'),
    ('Skysoar', 'vol'),
    ('Electraflare', 'électrique');
