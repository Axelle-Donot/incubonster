CREATE DATABASE IF NOT EXISTS incubonster_stockage
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

USE incubonster_stockage;

CREATE TABLE monstre (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(64),
    pv INT,
    attaque INT,
    defense INT,
    vitesse INT,
    number INT,
    joueur_id INT,
    type VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
