CREATE DATABASE IF NOT EXISTS incubonster_boutique
    DEFAULT CHARACTER SET utf8
    DEFAULT COLLATE utf8_general_ci;

USE incubonster_boutique;

CREATE TABLE joueur (
    id INT AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(64) UNIQUE NOT NULL,
    money INT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

CREATE TABLE oeuf (
    id INT AUTO_INCREMENT PRIMARY KEY,
    price INT,
    is_buy BOOLEAN
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


INSERT INTO oeuf (id, price, is_buy) VALUES (NULL, '10', '0');

