const { connection_stockage } = require("../dbConfig")
const stockageService = require("../services/storage.service")

//Récupère l'équipe de monstre du player
exports.getPlayerMonster = (req, res) => {
    const player_id = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    console.log("here", player_id)
  
    // Utiliser une requête préparée avec des paramètres
    connection_stockage.query(
      "SELECT * FROM monstre WHERE joueur_id = ?",
      [player_id],
      (err, results, fields) => {
        if (err) {
          console.error("Erreur lors de l'exécution de la requête :", err)
          return
        }
  
        // Traiter les résultats
        console.log("Résultats de la requête :", results)
        if (results.length > 0) {
          res.status(200).json(results)
        } else {
          res.status(200).json("pas de monstre avec ce joueur_id")
        }
      }
    )
  }
  
//ajoute un monstre à l'équipe
exports.addMonster = async (req, res) => {
    const dataFromQueue = await receiveQueue()
  
   
    const monster_nb = (await monstreService.getMonsterNumber(player_id)) + 1
  
    const data = {
      nom: monster_name,
      pv: monster_pv ?? monstreService.generateRandomNumber(), //Si pas de valeur alors en génère une
      attaque: monster_atk ?? monstreService.generateRandomNumber(), //Si pas de valeur alors en génère une
      defense: monster_def ?? monstreService.generateRandomNumber(), //Si pas de valeur alors en génère une
      vitesse: monster_vit ?? monstreService.generateRandomNumber(), //Si pas de valeur alors en génère une
      number: monster_nb,
      joueur_id: player_id,
      type: monster_type,
    }
  
    const { player_id, monster_name, monster_type, monster_pv, monster_atk, monster_def, monster_vit } = req.body // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    const formatData = {
      player_id : player_id,
      monster_name : monster_name,
      monster_type : monster_type,
      monster_pv: monster_pv,
      monster_atk:monster_atk,
      monster_def:monster_def,
      monster_vit:monster_vit
    }
    console.log(formatData)
    const status = await stockageService.addMonster(formatData)
    res.status(200).json(200)
  }