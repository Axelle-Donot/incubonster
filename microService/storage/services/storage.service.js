const { connection_stockage } = require("../dbConfig")

async function addMonster(dataSend) {
    try {
        console.log(dataSend)
        const monster_nb = 0

        const data = {
            nom: dataSend['monster_name'],
            pv: dataSend['monster_pv'] ?? generateRandomNumber(), //Si pas de valeur alors en génère une
            attaque: dataSend['monster_atk'] ?? generateRandomNumber(), //Si pas de valeur alors en génère une
            defense: dataSend['monster_def'] ?? generateRandomNumber(), //Si pas de valeur alors en génère une
            vitesse: dataSend['monster_vit'] ?? generateRandomNumber(), //Si pas de valeur alors en génère une
            number: monster_nb,
            joueur_id: dataSend['player_id'],
            type: dataSend['monster_type'],
        }

        console.log(data)

        // Utiliser une requête préparée avec des paramètres
        const [rows, fields] = await new Promise((resolve, reject) => {
            connection_stockage.query(
                "INSERT INTO monster SET ?",
                data,
                (err, results, fields) => {
                    if (err) {
                        console.error("Erreur lors de l'exécution de la requête :", err)
                        reject(err)
                    } else {
                        resolve([results, fields])
                    }
                }
            )
        })
        return 200
    } catch (error) {
        console.error('Erreur lors de l\'exécution de la requête 2:', error);
        return null;
    }

}

module.exports = {
   
    addMonster
};