const express = require("express")
const router = express.Router()
const storageController = require("../controllers/storageController")

router.get("/stokage/all", storageController.getAll)
router.post("/stokage/add/monster", storageController.addMonster)

module.exports = router
