const cors = require("cors")
const bodyParser = require("body-parser")
const express = require('express');
const app = express();

const port = process.env.PORT || 4001;

app.use(express.json());
app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.send('Hello World!');
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});