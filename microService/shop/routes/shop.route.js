const express = require("express")
const router = express.Router()
const shopController = require("../controllers/shopController")

router.get("/shop/oeuf", shopController.getEggs)
router.put("/shop/oeuf", shopController.updateEgg)
router.put("/shop/money", shopController.getMoney)

module.exports = router
