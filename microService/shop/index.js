const cors = require("cors")
const bodyParser = require("body-parser")
const express = require("express")
const app = express()
const shopRoutes = require("./routes/shop.route")

const port = process.env.PORT || 4005

app.use(express.json())
app.use(cors())
app.use(bodyParser.json())

// Importer les configurations de connexion à la base de données
const {
  connection_boutique,
  connection_incubateur,
  connection_joueur,
  connection_monstre,
  connection_stockage,
} = require("./dbConfig")

// Fonction de test de connexion à la base de données
function testDatabaseConnection(connection) {
  connection.connect(function (err) {
    if (err) {
      console.error("Error connecting to database:", err)
    } else {
      console.log("Connected to database")
    }
  })
}

// Tester les connexions à la base de données
testDatabaseConnection(connection_boutique)
testDatabaseConnection(connection_incubateur)
testDatabaseConnection(connection_joueur)
testDatabaseConnection(connection_monstre)
testDatabaseConnection(connection_stockage)

app.get("/", (req, res) => {
  res.send("Hello World!")
})
app.use(shopRoutes)


app.listen(port, () => {
  console.log(`Server is running on port ${port}`)
})

