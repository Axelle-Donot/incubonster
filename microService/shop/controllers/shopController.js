const { connection_boutique } = require("../dbConfig")

//Récupère les oeufs du shop
exports.getEggs = (req, res) => {
    const player_id = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    console.log("here", player_id)
  
    // Utiliser une requête préparée avec des paramètres
    connection_boutique.query(
      "SELECT * FROM oeuf",
      (err, results, fields) => {
        if (err) {
          console.error("Erreur lors de l'exécution de la requête :", err)
          return
        }
  
        // Traiter les résultats
        console.log("Résultats de la requête :", results)
        if (results.length > 0) {
          res.status(200).json(results)
        } else {
          res.status(200).json("pas d'oeuf dans le shop")
        }
      }
    )
  }
  
//Update les oeufs du shop
exports.updateEgg = (req, res) => {
    const egg_id = req.body['egg_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    const is_buy = req.body['is_buy'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    console.log("here", egg_id)
  
    // Utiliser une requête préparée avec des paramètres
    connection_boutique.query(
      "UPDATE oeuf SET is_buy = ? WHERE id = ?",
      [is_buy, egg_id],
      (err, results, fields) => {
        if (err) {
          console.error("Erreur lors de l'exécution de la requête :", err)
          return
        }
  
        // Traiter les résultats
        console.log("Résultats de la requête :", results)
       
        res.status(200).json("update reussite")
        
      }
    )
  }

//permet de get la value money du joueur
exports.getMoney = (req, res) => {
    const player_id = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  
    // Utiliser une requête préparée avec des paramètres
    connection_boutique.query(
      "SELECT money FROM joueur WHERE id = ?",
      [player_id],
      (err, results, fields) => {
        if (err) {
          console.error("Erreur lors de l'exécution de la requête :", err)
          return
        }
  
        // Traiter les résultats
        console.log("Résultats de la requête :", results)
        if (results.length > 0) {
          res.status(200).json(results)
        } else {
          res.status(200).json("erreur pas de money recupéré", 200)
        }
      }
    )
  }

//permet d'update la value money du joueur
exports.updateMoney = (req, res) => {
    const { money } = req.body // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    const player_id = req.headers['player_id']
    // Utiliser une requête préparée avec des paramètres
    connection_boutique.query(
      "UPDATE joueur SET money = ? WHERE id = ?",
      [money, player_id],
      (err, results, fields) => {
        if (err) {
          console.error("Erreur lors de l'exécution de la requête :", err)
          return
        }
        res.status(200).json(200)
      }
    )
  }