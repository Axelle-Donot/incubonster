const express = require("express")
const router = express.Router()
const oeufController = require("../controllers/oeufController")

/**
 * @swagger
 * /oeuf:
 *   get:
 *     summary: Return the eggs
 *     description: return all the egg of a player
 *     tags:
 *      - Player
 *     responses:
 *       200:
 *          description: 'array of egg'
 */
router.get("/player/oeuf", oeufController.getEggs)

/**
 * @swagger
 * /oeuf:
 *   post:
 *     summary: Create an egg
 *     description: Create a new egg in the inventory off a player
 *     tags:
 *      - Player
 *     responses:
 *       200:
 *          description: '200'
 */
router.post("/player/oeuf", oeufController.addEggs) //permet de créer un oeuf dans l'inventaire du player, headers : {"player_id":"1"}
module.exports = router
