const express = require("express")
const router = express.Router()
const playerController = require("../controllers/playerController")

/**
 * @swagger
 * /player:
 *   post:
 *     summary: login
 *     description: login
 *     tags:
 *      - Player
 *     responses:
 *       200:
 *          description: '200'
 */
router.post("/player/login", playerController.login)

/**
 * @swagger
 * /player:
 *   get:
 *     summary: Return money
 *     description: Allow to return the money of a player
 *     tags:
 *      - Player
 *     responses:
 *       200:
 *          description: The amount of money of the player
 */
router.get("/player/money", playerController.getMoney)

/**
 * @swagger
 * /player:
 *   put:
 *     summary: Update the money
 *     description: Update the amount of money of a player the body is the new value of money
 *     tags:
 *      - Player
 *     responses:
 *       200:
 *          description: '200'
 */
router.put("/player/money", playerController.updateMoney)

module.exports = router
