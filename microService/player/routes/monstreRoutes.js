const express = require("express")
const router = express.Router()
const monstreController = require("../controllers/monstreController")

/**
 * @swagger
 * /monstre:
 *   get:
 *     summary: Return la team
 *     description: Return all the player team of monster
 *     tags:
 *      - Player
 *     responses:
 *       200:
 *          description: Array of monster
 */
router.get("/player/team", monstreController.getPlayerMonster)

/**
 * @swagger
 * /monstre:
 *   get:
 *     summary: Check if can add to team
 *     description: check if a player can add a monster to his team
 *     tags:
 *      - Player
 *     responses:
 *       200:
 *          description: True or False
 */
router.get("/player/canAdd/monster", monstreController.canAddMonster)

/**
 * @swagger
 * /monstre:
 *   post:
 *     summary: Add a monster
 *     description: Add a monster in the player team
 *     tags:
 *      - Player
 *     responses:
 *       200:
 *          description: '200'
 */
router.post("/player/add/monster", monstreController.addMonster)

/* Permet d'ajouter un monstre à l'équipe, 
body : {
    "player_id":"1", => OBLIGATOIRE
    "monster_name" : "Flareonix", => OBLIGATOIRE
    "monster_type" : "feu", => OBLIGATOIRE
    "monster_pv": 100, => si pas transmis ou null une value sera générée
    "monster_atk": 100, => si pas transmis ou null une value sera générée
    "monster_def": 100, => si pas transmis ou null une value sera générée
    "monster_vit": 100, => si pas transmis ou null une value sera générée
}
*/

module.exports = router
