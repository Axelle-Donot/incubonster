const { connection_joueur } = require("../dbConfig")

//Genere un nombre entier entre 1 et 24 compris
function generateRandomNumber() {
  // Math.floor arrondit vers le bas pour obtenir un entier
  return Math.floor(Math.random() * 24) + 1
}

//permet de get tous les oeufs de l'inventaire du joueur
exports.getEggs = (req, res) => {
  const player_id  = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".

  // Utiliser une requête préparée avec des paramètres
  connection_joueur.query(
    "SELECT * FROM oeuf WHERE joueur_id = ?",
    [player_id],
    (err, results, fields) => {
      if (err) {
        console.error("Erreur lors de l'exécution de la requête :", err)
        return
      }

      // Traiter les résultats
      console.log("Résultats de la requête :", results)
      if (results.length > 0) {
        res.status(200).json(results)
      } else {
        res.status(200).json("pas d'oeuf avec ce player_id", 200)
      }
    }
  )
}
//permet de add un oeufs à l'inventaire du joueur
exports.addEggs = (req, res) => {
  const player_id  = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  const currentDate = new Date() // ou utilisez la date que vous souhaitez insérer
  const currentDateOfIncu = new Date(currentDate)
  const timeToAdd = generateRandomNumber()
  currentDateOfIncu.setHours(currentDate.getHours() + timeToAdd + 1)
  console.log(currentDateOfIncu, timeToAdd)

  // Formater la date en format DATETIME pour MySQL
  const timeOfIncubation = currentDateOfIncu
    .toISOString()
    .slice(0, 19)
    .replace("T", " ")
  const data = {
    joueur_id: player_id,
    timeOfIncubation: timeOfIncubation, //format 2024-02-06 13:20:52
  }

  // Utiliser une requête préparée avec des paramètres
  connection_joueur.query(
    "INSERT INTO oeuf SET ?",
    data,
    (err, results, fields) => {
      if (err) {
        console.error("Erreur lors de l'exécution de la requête :", err)
        return
      }
    }
  )
  res.status(200).json(200)
}
