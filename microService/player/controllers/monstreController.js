const { connection_joueur } = require("../dbConfig")
const monstreService = require("../services/monstreService")
const amqp = require("amqplib")

//lance la receive de la queue
//receiveQueue().then(() => console.log('Queue receiving started.')).catch(err => console.error('Error starting queue receiving:', err));


async function receiveQueue() {
  try {
    // Connexion à RabbitMQ
    const connection = await amqp.connect("amqp://rabbitmq")
    const channel = await connection.createChannel()

    // Déclaration de la file d'attente
    const queueName = "monsterQueue"
    await channel.assertQueue(queueName, { durable: false })

    console.log("Waiting for messages in the queue...")

    // Écoute des messages dans la file d'attente
    channel.consume(queueName, async (msg) => {
      const data = msg.content
      console.log(data)
      // Traitez le message comme vous le souhaitez ici
      const status = await monstreService.addMonster(data)

      // Acknowledge le message pour le supprimer de la file d'attente
      channel.ack(msg)
    })
  } catch (error) {
    console.error("Error receiving message from queue:", error)
  }
}

//Récupère l'équipe de monstre du player
exports.getPlayerMonster = (req, res) => {
  const player_id = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  console.log("here", player_id)

  // Utiliser une requête préparée avec des paramètres
  connection_joueur.query(
    "SELECT * FROM monster WHERE joueur_id = ?",
    [player_id],
    (err, results, fields) => {
      if (err) {
        console.error("Erreur lors de l'exécution de la requête :", err)
        return
      }

      // Traiter les résultats
      console.log("Résultats de la requête :", results)
      if (results.length > 0) {
        res.status(200).json(results)
      } else {
        res.status(200).json("pas de monstre avec ce joueur_id")
      }
    }
  )
}

//Verifie si le player peut ajouter un monstre à son équipe
exports.canAddMonster = (req, res) => {
  const player_id = req.headers["player_id"] // Utilise "req.query" pour récupérer le paramètre d'URL "id".

  // Utiliser une requête préparée avec des paramètres
  connection_joueur.query(
    "SELECT COUNT(*) AS count    FROM monster WHERE joueur_id = ?",
    [player_id],
    (err, results, fields) => {
      if (err) {
        console.error("Erreur lors de l'exécution de la requête :", err)
        return
      }
      const count = results[0].count

      // Traiter les résultats
      console.log("Résultats de la requête :", count)
      if (count < 6) {
        res.status(200).json(true)
      } else {
        res.status(200).json(false)
      }
    }
  )
}


//ajoute un monstre à l'équipe
exports.addMonster = async (req, res) => {
  const dataFromQueue = await receiveQueue()

 
  const monster_nb = (await monstreService.getMonsterNumber(player_id)) + 1

  const data = {
    nom: monster_name,
    pv: monster_pv ?? monstreService.generateRandomNumber(), //Si pas de valeur alors en génère une
    attaque: monster_atk ?? monstreService.generateRandomNumber(), //Si pas de valeur alors en génère une
    defense: monster_def ?? monstreService.generateRandomNumber(), //Si pas de valeur alors en génère une
    vitesse: monster_vit ?? monstreService.generateRandomNumber(), //Si pas de valeur alors en génère une
    number: monster_nb,
    joueur_id: player_id,
    type: monster_type,
  }

  const { player_id, monster_name, monster_type, monster_pv, monster_atk, monster_def, monster_vit } = req.body // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  const formatData = {
    player_id : player_id,
    monster_name : monster_name,
    monster_type : monster_type,
    monster_pv: monster_pv,
    monster_atk:monster_atk,
    monster_def:monster_def,
    monster_vit:monster_vit
  }
  console.log(formatData)
  const status = await monstreService.addMonster(formatData)
  res.status(200).json(200)
}
