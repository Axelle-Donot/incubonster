//import { connection_joueur } from "../../dbConfig";
// const { connection_joueur } = require("../dbConfig");

//Permet de s'identifier en tant que joueur d'après le pseudo
const {connection_joueur} = require("../dbConfig");
exports.login = (req, res) => {
  const { pseudo } = req.body // Utilise "req.query" pour récupérer le paramètre d'URL "id".

  // Utiliser une requête préparée avec des paramètres
  connection_joueur.query(
    "SELECT * FROM joueur WHERE pseudo = ?",
    [pseudo],
    (err, results, fields) => {
      if (err) {
        console.error("Erreur lors de l'exécution de la requête :", err)
        return
      }

      // Traiter les résultats
      console.log("Résultats de la requête :", results)
      if (results.length > 0) {
        res.status(200).json(results)
      } else {
        res.status(200).json("pas de joueur avec ce pseudo", 200)
      }
    }
  )
}

//permet de get la value money du joueur
exports.getMoney = (req, res) => {
  const player_id = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".

  // Utiliser une requête préparée avec des paramètres
  connection_joueur.query(
    "SELECT money FROM joueur WHERE id = ?",
    [player_id],
    (err, results, fields) => {
      if (err) {
        console.error("Erreur lors de l'exécution de la requête :", err)
        return
      }

      // Traiter les résultats
      console.log("Résultats de la requête :", results)
      if (results.length > 0) {
        res.status(200).json(results)
      } else {
        res.status(200).json("erreur pas de money recupéré", 200)
      }
    }
  )
}
//permet d'update la value money du joueur
exports.updateMoney = (req, res) => {
  const { money } = req.body // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  const player_id = req.headers['player_id']
  // Utiliser une requête préparée avec des paramètres
  connection_joueur.query(
    "UPDATE joueur SET money = ? WHERE id = ?",
    [money, player_id],
    (err, results, fields) => {
      if (err) {
        console.error("Erreur lors de l'exécution de la requête :", err)
        return
      }
      res.status(200).json(200)
    }
  )
}
