const mysql = require("mysql2")

// ------------Local
// const connection_boutique = mysql.createConnection({
//   host: "localhost",
//   user: "root", // root on xampp or wamp
//   password: "", // empty for xampp or wamp
//   database: "incubonster_boutique",
//   port: 3306,
// })
// const connection_incubateur = mysql.createConnection({
//   host: "localhost",
//   user: "root", // root on xampp or wamp
//   password: "", // empty for xampp or wamp
//   database: "incubonster_incubateur",
//   port: 3306,
// })
// const connection_joueur = mysql.createConnection({
//   host: "localhost",
//   user: "root", // root on xampp or wamp
//   password: "", // empty for xampp or wamp
//   database: "incubonster_joueur",
//   port: 3306,
// })
// const connection_monstre = mysql.createConnection({
//   host: "localhost",
//   user: "root", // root on xampp or wamp
//   password: "", // empty for xampp or wamp
//   database: "incubonster_monstre",
//   port: 3306,
// })
// const connection_stockage = mysql.createConnection({
//   host: "localhost",
//   user: "root", // root on xampp or wamp
//   password: "", // empty for xampp or wamp
//   database: "incubonster_stockage",
//   port: 3306,
// })

// ------------Docker
const connection_boutique = mysql.createConnection({
  host: "MySqlBoutique",
  user: "root",
  password: "root",
  database: "incubonster_boutique",
})
const connection_incubateur = mysql.createConnection({
  host: "MySqlIncubateur",
  user: "root",
  password: "root",
  database: "incubonster_incubateur",
})
const connection_joueur = mysql.createConnection({
  host: "MySqlJoueur",
  user: "root",
  password: "root",
  database: "incubonster_joueur",
})
const connection_monstre = mysql.createConnection({
  host: "MySqlMonstre",
  user: "root",
  password: "root",
  database: "incubonster_monstre",
})
const connection_stockage = mysql.createConnection({
  host: "MySqlStockage",
  user: "root",
  password: "root",
  database: "incubonster_stockage",
})

module.exports = {
  connection_boutique,
  connection_incubateur,
  connection_joueur,
  connection_monstre,
  connection_stockage,
}
