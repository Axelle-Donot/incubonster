const { connection_joueur } = require("../dbConfig");

//Genere un nombre entier entre 1 et 100 compris
function generateRandomNumber() {
    // Math.floor arrondit vers le bas pour obtenir un entier
    return Math.floor(Math.random() * 100) + 1;
}

//Récupère la place du dernier monstre de l'équipe
async function getMonsterNumber(player_id) {
    try {
        const [rows, fields] = await new Promise((resolve, reject) => {
            connection_joueur.query('SELECT number FROM monster WHERE joueur_id = ? ORDER BY number DESC', [player_id], (err, results, fields) => {
                if (err) {
                    reject(err);
                } else {
                    resolve([results, fields]);
                }
            });
        });

        if (rows.length > 0) {
            return rows[0].number;
        } else {
            return 0;
        }
    } catch (error) {
        console.error('Erreur lors de l\'exécution de la requête :', error);
        return null;
    }
}

async function addMonster(dataSend) {
    try {
        console.log(dataSend)
        const monster_nb = await getMonsterNumber(dataSend['player_id']) + 1

        const data = {
            nom: dataSend['monster_name'],
            pv: dataSend['monster_pv'] ?? generateRandomNumber(), //Si pas de valeur alors en génère une
            attaque: dataSend['monster_atk'] ?? generateRandomNumber(), //Si pas de valeur alors en génère une
            defense: dataSend['monster_def'] ?? generateRandomNumber(), //Si pas de valeur alors en génère une
            vitesse: dataSend['monster_vit'] ?? generateRandomNumber(), //Si pas de valeur alors en génère une
            number: monster_nb,
            joueur_id: dataSend['player_id'],
            type: dataSend['monster_type'],
        }

        console.log(data)

        // Utiliser une requête préparée avec des paramètres
        const [rows, fields] = await new Promise((resolve, reject) => {
            connection_joueur.query(
                "INSERT INTO monster SET ?",
                data,
                (err, results, fields) => {
                    if (err) {
                        console.error("Erreur lors de l'exécution de la requête :", err)
                        reject(err)
                    } else {
                        resolve([results, fields])
                    }
                }
            )
        })
        return 200
    } catch (error) {
        console.error('Erreur lors de l\'exécution de la requête 2:', error);
        return null;
    }

}

module.exports = {
    generateRandomNumber,
    getMonsterNumber,
    addMonster
};