const amqp = require("amqplib")
const { connection_monstre } = require("../dbConfig")

// Fonction pour envoyer des informations à la file d'attente
async function sendQueue(dataMonstre) {
  try {
    // Connexion à RabbitMQ
    const connection = await amqp.connect("amqp://rabbitmq")
    const channel = await connection.createChannel()

    // Déclaration de la file d'attente
    const queueName = `monsterQueue`
    await channel.assertQueue(queueName, { durable: false })

    // Envoi du message à la file d'attente
    channel.sendToQueue(queueName, Buffer.from(JSON.stringify(dataMonstre)))
    console.log(`[x] Message envoyé: ${dataMonstre}`)

    // Fermeture de la connexion
    // setTimeout(() => {
    //   connection.close()
    //   process.exit(0)
    // }, 500)
  } catch (error) {
    console.error(error)
    processexit(1)
  }
}

async function receiveQueue() {
  try {
    // Connexion à RabbitMQ
    const connection = await amqp.connect("amqp://rabbitmq")
    const channel = await connection.createChannel()

    // Déclaration de la file d'attente
    const queueName = "monsterQueue"
    await channel.assertQueue(queueName, { durable: false })

    console.log("Waiting for messages in the queue...")

    // Écoute des messages dans la file d'attente
    channel.consume(queueName, (msg) => {
      const message = msg.content.toString()
      console.log(`Received message: ${message}`)
      // Traitez le message comme vous le souhaitez ici
      //get un monstre au hasard et faire un service createmonstre auquel tu envoies les infos
      //faire passe plat avec le addtoQ

      // Acknowledge le message pour le supprimer de la file d'attente
      channel.ack(msg)
    })
  } catch (error) {
    console.error("Error receiving message from queue:", error)
  }
}

exports.addToQ = async (req, res) => {
  const { monster_name, monster_type } = req.body // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  const player_id = req.headers['player_id']
  const dataMonstre = {
    nom: monster_name,
    joueur_id: player_id,
    type: monster_type,
  }

  console.log(dataMonstre)

  await sendQueue(dataMonstre)

  res.status(200).json(200)
}
