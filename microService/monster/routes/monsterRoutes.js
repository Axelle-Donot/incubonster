const express = require("express")
const router = express.Router()
const monsterController = require("../controllers/monsterController")

/**
 * @swagger
 * /monster:
 *   post:
 *     summary: Add to queue
 *     description: add a monster to a rabbitMQ
 *     tags:
 *      - Monstre
 *     responses:
 *       200:
 *          description: 200
 */
router.post("/monster/addToQ", monsterController.addToQ) //Ajoute un monstre à la RabbitQ, body : {"monster_name":"Flareonix", "monster_type":"feu"}, headers : {"player_id":1}
module.exports = router
