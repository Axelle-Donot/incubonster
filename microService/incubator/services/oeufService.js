const { connection_incubateur } = require("../dbConfig");
const incubatorService = require("../services/incubatorService");


// recupère tous les oeufs en cours d'incubation
function getAllEggs(player_id) {
  return new Promise((resolve, reject) => {
    // Utiliser une requête préparée avec des paramètres
    connection_incubateur.query('SELECT * FROM oeuf WHERE joueur_id = ?', [player_id], (err, results, fields) => {
      if (err) {
        console.error('Erreur lors de l\'exécution de la requête :', err);
        reject(err);
        return;
      }

      // Traiter les résultats
      //console.log('Résultats de la requête :', results, results.length);
      if (results.length > 0) {
        resolve(results);
      } else {
        resolve(null);
      }
    });
  });
}

//verifie si on peut ajouter un oeuf dans un incubateur
async function canAddEggs(player_id) {
  const NumberOfIncubateur = await incubatorService.getNumberOfIncubateur(player_id);
  const listOfEggsIncubing = await getAllEggs(player_id);
  if (NumberOfIncubateur-listOfEggsIncubing.length > 0){
    return true
  }else{
    return false
  }

}

module.exports = {
  getAllEggs,
  canAddEggs,
};