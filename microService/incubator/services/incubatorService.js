const { connection_incubateur } = require("../dbConfig")
const oeufService = require("../services/oeufService")
const amqp = require("amqplib")

// Fonction pour envoyer des informations à la file d'attente
async function sendQueue(dataMonstre) {
  try {
    // Connexion à RabbitMQ
    const connection = await amqp.connect("amqp://rabbitmq")
    const channel = await connection.createChannel()

    // Déclaration de la file d'attente
    const queueName = `monsterQueue`
    await channel.assertQueue(queueName, { durable: false })

    // Envoi du message à la file d'attente
    channel.sendToQueue(queueName, Buffer.from(JSON.stringify(dataMonstre)))
    console.log(`[x] Message envoyé: ${dataMonstre}`)

    // Fermeture de la connexion
    // setTimeout(() => {
    //   connection.close()
    //   process.exit(0)
    // }, 500)
  } catch (error) {
    console.error(error)
    processexit(1)
  }
}

// Fonction qui check si un oeuf a fini d'incuber
async function checkIncubation(player_id) {
  console.log("La fonction s'exécute continuellement.", player_id)
  const listOeuf = await oeufService.getAllEggs(player_id)
  listOeuf.forEach((e) => {
    const dateOeuf = new Date(e.timeOfIncubation)
    const now = new Date()

    console.log(now, dateOeuf)
    //si l'oeuf a fini
    if (e.timeOfIncubation < now) {
      sendQueue(e)
      console.log("l'oeuf a finito !!", e)
    }
  })
}

//retourn le nombre d'incubateur
function getNumberOfIncubateur(player_id) {
  return new Promise((resolve, reject) => {
    connection_incubateur.query(
      "SELECT number FROM incubateur WHERE joueur_id = ?",
      [player_id],
      (err, results, fields) => {
        if (err) {
          console.error("Erreur lors de l'exécution de la requête :", err)
          return
        }

        // Traiter les résultats
        if (results.length > 0) {
          resolve(results[0].number)
        } else {
          resolve(null)
        }
      }
    )
  })
}

module.exports = {
  checkIncubation,
  getNumberOfIncubateur,
}
