const cors = require("cors")
const bodyParser = require("body-parser")
const express = require("express")

const app = express()
const incubatorRoutes = require("./routes/incubatorRoutes")
const oeufRoutes = require("./routes/oeufRoutes")
const swaggerJsdoc = require("swagger-jsdoc")
const swaggerUi = require("swagger-ui-express")

const port = process.env.PORT || 4002

// Swagger setup
const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Incubator Microservice",
      version: "1.0.0",
      description: "Documentation for Incubator Microservice",
    },
  },
  apis: ["./routes/*.js"], // Specify the path to your route files
}

const specs = swaggerJsdoc(options)

// Serve Swagger UI
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs))

app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json")
  res.send(specs)
})

app.use(express.json())
app.use(cors())
app.use(bodyParser.json())

// Importer les configurations de connexion à la base de données
const {
  connection_boutique,
  connection_incubateur,
  connection_joueur,
  connection_monstre,
  connection_stockage,
} = require("./dbConfig")

// Fonction de test de connexion à la base de données
function testDatabaseConnection(connection) {
  connection.connect(function (err) {
    if (err) {
      console.error("Error connecting to database:", err)
    } else {
      console.log("Connected to database")
    }
  })
}

// Tester les connexions à la base de données
testDatabaseConnection(connection_boutique)
testDatabaseConnection(connection_incubateur)
testDatabaseConnection(connection_joueur)
testDatabaseConnection(connection_monstre)
testDatabaseConnection(connection_stockage)

app.get("/", (req, res) => {
  res.send("Hello World!")
})
app.use(incubatorRoutes)
app.use(oeufRoutes)

app.listen(port, () => {
  console.log(`Server is running on port ${port}`)
})
