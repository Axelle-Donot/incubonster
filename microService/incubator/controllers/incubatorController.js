const { connection_incubateur} = require("../dbConfig");
const cron = require('node-cron');
const incubatorService = require("../services/incubatorService");
let cronTask;

//fonction qui lance le cron qui check si un oeuf a fini d'incuber
exports.StartChecking = (req, res) => {
  const player_id  = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".

  const checkIncubationCall = () => {
    incubatorService.checkIncubation(player_id);
  };


  // Définir la planification pour exécuter la fonction toutes les secondes
  cronTask = cron.schedule('* * * * * *', checkIncubationCall, {
    scheduled: false, // Désactivez l'exécution automatique au démarrage
  });

  // Démarrer la planification
  cronTask.start();
  res.status(200).json(200)
}
//stop le cron
exports.StopChecking = (req, res) => {
  if (cronTask) {
    console.log('Le cron est arrêté.');
    cronTask.stop(); // Arrêter la tâche cron
  } else {
    console.log('Le cron n\'est pas encore démarré.');
  }
  res.status(200).json(200)

}





//fonciton qui ajoute un incubateur
exports.addIncu = async (req, res) => {
  //ajoute un incu au joueur
  const player_id = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  const nbIncu = await incubatorService.getNumberOfIncubateur(player_id);

  // Utiliser une requête préparée avec des paramètres
  connection_incubateur.query('UPDATE incubateur SET number = ? WHERE joueur_id = ?', [nbIncu + 1, player_id], (err, results, fields) => {
    if (err) {
      console.error('Erreur lors de l\'exécution de la requête :', err);
      return;
    }
  });
  res.status(200).json(200)
}

//fonction qui check si on peut ajouter un incubateur
exports.canAddIncu =  async (req, res)=> {
  const player_id = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
  const nbIncu = await incubatorService.getNumberOfIncubateur(player_id);

  if(nbIncu < 6){
    res.status(200).json(true)
  }else{
    res.status(200).json(false)
  }

}



