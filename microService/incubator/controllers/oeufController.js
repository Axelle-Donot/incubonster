const { connection_incubateur, connection_joueur} = require("../dbConfig");
const oeufService = require("../services/oeufService");




//fonction qui recupère tous les oeufs qui incumbe
exports.getAllEggs = async (req, res) => {
    try{
    const player_id = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    let ret = await oeufService.getAllEggs(player_id);
    res.status(200).json(ret)
}catch (error) {
    console.error('Erreur lors de la récupération des œufs :', error);
    res.status(500).json({ error: 'Erreur lors de la récupération des œufs.' });
}

}

//fonciton qui check si on peut ajouter un oeuf à incuber
exports.canAddEggs = async (req, res) => {
    const player_id  = req.headers['player_id'] // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    ret = await oeufService.canAddEggs(player_id);
    res.status(200).json(ret)

}

//fonction qui ajoute un oeuf a incuber
exports.addEggs = (req,res) => {
    //ajoute un oeuf à l'incu
    const { timeOfIncubation } = req.body // Utilise "req.query" pour récupérer le paramètre d'URL "id".
    const player_id = req.headers['player_id']
    const data = {
        joueur_id: player_id,
        timeOfIncubation: timeOfIncubation
    }

    // Utiliser une requête préparée avec des paramètres
    connection_joueur.query('INSERT INTO oeuf SET ?', data, (err, results, fields) => {
        if (err) {
            console.error('Erreur lors de l\'exécution de la requête :', err);
            return;
        }
    });
    res.status(200).json(200)


}
