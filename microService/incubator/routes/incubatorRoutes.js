const express = require("express")
const router = express.Router()
const IncubatorController = require("../controllers/incubatorController")

/**
 * @swagger
 * /incubator:
 *   get:
 *     summary: Start Cron job
 *     description: Start the cron job for the incubator
 *     tags:
 *      - Incubator
 *     responses:
 *       200:
 *          description: 200
 */
router.get(
  "/incubator/checkIncubation/start",
  IncubatorController.StartChecking
)

/**
 * @swagger
 * /incubator:
 *   get:
 *     summary: Stop the cron job
 *     description: only for developpement
 *     tags:
 *      - Incubator
 *     responses:
 *       200:
 *          description: 200
 */
router.get("/incubator/checkIncubation/stop", IncubatorController.StopChecking)

/**
 * @swagger
 * /incubator:
 *   post:
 *     summary: Add incubator to player
 *     description: when a player buy an incubator is add to the other already in his possesion
 *     tags:
 *      - Incubator
 *     responses:
 *       200:
 *          description: 200
 */
router.post("/incubator/add", IncubatorController.addIncu)

/**
 * @swagger
 * /incubator:
 *   get:
 *     summary: check number of incubator of a player
 *     description: Before adding a new incubator is add to a player, we verify if he has less than 6 incubator
 *     tags:
 *      - Incubator
 *     responses:
 *       200:
 *         description: True or Fasle
 */
router.get("/incubator/canAdd", IncubatorController.canAddIncu)

module.exports = router
