const express = require("express")
const router = express.Router()
const oeufController = require("../controllers/oeufController")

/**
 * @swagger
 * /oeuf:
 *   get:
 *     summary: Retrive egg that are incubating
 *     description: Retrive all the incubating egg of a player
 *     tags:
 *      - Incubator
 *     responses:
 *       200:
 *         description: Array of egg
 */
router.get("/incubator/oeuf", oeufController.getAllEggs)

/**
 * @swagger
 * /oeuf:
 *   get:
 *     summary: check if we can add an egg to incubate
 *     description: Before adding a new egg in a incubator, we verify if he has less than 6 egg incubating
 *     tags:
 *      - Incubator
 *     responses:
 *       200:
 *         description: True or False
 */
router.get("/incubator/canAdd/oeuf", oeufController.canAddEggs)

/**
 * @swagger
 * /oeuf:
 *   post:
 *     summary: Add an egg to incubating
 *     description: Add an egg to incubating, whith in the body the time of incubation
 *     tags:
 *      - Incubator
 *     responses:
 *       200:
 *          description: 200
 */
router.post("/incubator/oeuf/add", oeufController.addEggs) //ajoute un oeuf à incuber, headers : {"player_id":"1"}, body : {"timeOfIncubation": "2024-02-06 14:20:52"}

module.exports = router
