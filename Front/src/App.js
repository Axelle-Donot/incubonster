import "./App.css";
import LandingPage from "./Pages/LandingPage";
import {BrowserRouter as Router, Navigate, Route, Routes} from 'react-router-dom';
import Login from "./Pages/Login";
import Shop from "./Pages/Shop";
import Player from "./Pages/Player";
import Monsters from "./Pages/Monsters";
import Header from "./Pages/Header";
import Eggs from "./Pages/Eggs";

const isAuth = () => {
    return sessionStorage.getItem("name") !== null;
};

function App() {
  return (
    <div className="App">
        <Header/>
        <Router>
            <Routes>
                <Route path="/" element={isAuth() ? <LandingPage /> : <Navigate to="/login" />} />
                <Route path="/login" element={<Login />} />
                {/* Utilisez GuardedRoute pour les chemins nécessitant une authentification */}
                <Route path="/shop" element={isAuth() ? <Shop /> : <Navigate to="/login" />} />
                <Route path="/player" element={isAuth() ? <Player /> : <Navigate to="/login" />} />
                <Route path="/monsters" element={isAuth() ? <Monsters /> : <Navigate to="/login" />} />
                <Route path="/eggs" element={isAuth() ? <Eggs /> : <Navigate to="/login" />} />
            </Routes>
        </Router>
    </div>
  );
}

export default App;
