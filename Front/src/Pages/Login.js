import React, {useEffect, useState} from "react";
import "./css/Sign.css";
import { useNavigate } from "react-router-dom";
import {login} from "../services/player.service";


const Login = () => {
    const [username, setUsername] = useState("");
    const navigate = useNavigate()

    const onLogin = async () => {
        const res = await login(username);
        if(res.status === 200){
            const name = res.data[0].pseudo;
            const money = res.data[0].money + 100;
            const id = res.data[0].id;
            console.log(money);
            sessionStorage.setItem("name", name);
            sessionStorage.setItem("money", money);
            sessionStorage.setItem("id", id);
            navigate("/")
        }

    }

    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };

    return (
        <div className={"login"}>
            <form>
                <div className={"login-title"}>
                    <img src={"/img/title.png"} alt={"incubateur"} className={"incubator"}/>
                </div>
                <div className={"login-wrapper"}>
                    <div className={"login-image"}>
                        <img src={"/img/incubateur.png"} alt={"incubateur"} className={"incubator"}/>
                    </div>
                    <div>
                        <input type="text" placeholder='Pseudo' value={username}
                               onChange={handleUsernameChange}/>
                    </div>
                    <button className="createAccount" type="button" onClick={onLogin}>Login</button>
                </div>
            </form>
        </div>
    );
};

export default Login;
