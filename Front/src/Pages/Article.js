import React, {useEffect} from "react";
import "./css/Shop.css";

const Article = ({ id, price, onBuy,enoughMoney }) => {

    useEffect(() => {
        console.log(enoughMoney)
    }, []);

    return (
        <div className={"pokemon-article"}>
            <div className={"pokemon-article-wrapper"}>
                <div className={"pokemon-article-color"}></div>
                <img className={"article-image"} src={`/img/egg${id}.png`} alt={"egg"}/>
                <h1 className={"article-title"}>Oeuf</h1>
                <div className={"article-price"}>
                    <p>{price}</p>
                    <img className={"article-price-image"} src={"/img/coin.png"} alt={"coin"}/>
                </div>
                <button disabled={!enoughMoney} className={"article-button"} onClick={() => onBuy(price)}>Acheter</button>
            </div>
        </div>
    );
};

export default Article;