import React, {useEffect} from "react";
import "./css/Player.css";
import {useNavigate} from "react-router-dom";

const Player = () => {
    const navigate = useNavigate()
    const onReturn = () => {
        navigate("/");
    }

    return (
        <div className="player-wrapper">
            <div className="player-info">
                <div className="player-description">
                    <div className="player-title">
                        <div className="player-header">
                            <img src="/img/incubateur.png" alt="incubateur" style={{width: '60px', height: "auto"}}/>
                            <h1>CARTE D'ÉLÉVEUR</h1>
                            <img src="/img/incubateur.png" alt="incubateur" style={{width: '60px', height: "auto"}}/>
                        </div>
                    </div>
                    <div className="player-body">
                        <p>Nom : {sessionStorage.getItem("name")}</p>
                    </div>
                    <div>
                        <button onClick={onReturn}>Retour</button>
                    </div>
                </div>
                <div className="player-picture">
                    <img src="/img/goku.png" alt="goku"/>
                </div>
            </div>
        </div>
    );
};

export default Player;
