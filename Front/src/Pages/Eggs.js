import React, {useCallback, useEffect, useState} from 'react';
import './css/Eggs.css';
import '../services/player.service';
import {getPlayerEggs} from "../services/player.service";
import "../services/incubatorService";
import {startCheckIncubation, getAllEggs} from "../services/incubatorService";


const Eggs = () => {
    const [eggs, setEggs] = useState([]);
    useEffect(() => {
        const fetchData = async() => {
            const player_id = +sessionStorage.getItem("id")
            const egg = await getAllEggs(player_id);
            if(egg.status === 200){
                let value = []
                if(Array.isArray(egg.data)){
                    value = egg.data;
                }
                setEggs(value)
            }
        }
        fetchData();
    }, []);
    const onClickEgg = async (id) => {
        setEggs(currentEggs =>
            currentEggs.map(async egg => {
                if (egg.id === id) {
                    const player_id = +sessionStorage.getItem("id");
                    await startCheckIncubation(player_id);
                    // Si l'œuf est déjà en incubation, afficher une alerte ou une logique similaire
                    if (egg.isIncubed) {
                        console.log("Cet œuf est déjà en incubation.");
                        return egg; // Retourne l'œuf sans changement
                    }
                    // Si l'œuf n'est pas en incubation, préparer les modifications
                    const newEgg = {...egg, isIncubed: !egg.isIncubed};

                    newEgg.image = '/img/incubateur_triangle_vert.png'; // Mise à jour de l'image
                    return newEgg;
                }
                return egg; // Retourne les œufs non concernés sans changement
            })
        );
    }

    return(
        <div className="eggs-main-container">
            <div className="eggs-container">
                {eggs.map((egg) => (
                    <div key={egg.id} className="egg-card" onClick={() => onClickEgg(egg.id)}>
                        <img src={'/img/triangle_vert.png'} alt={egg.name} className="egg-image"/>
                        <div className="egg-name">{egg.name}</div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Eggs;