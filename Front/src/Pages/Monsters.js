import React, {useEffect, useState} from "react";
import "./css/Monsters.css";
import "../services/monstre.service";

const Monsters = () => {
    const [monsters, setMonsters] = useState();

    useEffect(() => {
        const fetchData = async()=> {

        }
        fetchData();
    }, []);


    return (
        <>
            <div className={"monsters-list"}>
                {monsters && monsters.map((monster, index) => (
                    <div key={monster.id} className={"monster"}>
                        <div style={{ padding: '3px' }}>
                            <h1 style={{ textAlign: 'left' }}>#{monster.number}</h1>
                        </div>
                        <div className={"monsters-header"}>
                            {/* Remplacer par le chemin d'accès à l'image du monstre */}
                            <img className={"monster-image"} src={`/img/bulbasaur.webp`} alt={monster.name}/>
                        </div>
                        <div className={"monsters-body"}>
                            <p>{monster.name}</p>
                            <button>Relâcher</button>
                            <button>Mettre au stockage</button>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );
};

export default Monsters;