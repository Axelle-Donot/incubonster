import React, {useEffect, useState} from "react";
import "./css/Shop.css";

const Header = () => {
    // État pour le nom et l'argent
    const [name, setName] = useState('');
    const [money, setMoney] = useState('');

    useEffect(() => {
        // Fonction pour mettre à jour le nom et l'argent à partir de sessionStorage
        const updateState = () => {
            setName(sessionStorage.getItem("name") || '');
            setMoney(sessionStorage.getItem("money") || '');
        };

        updateState(); // Mettre à jour immédiatement lors du montage

        // Mettre en place un intervalle pour vérifier les mises à jour
        const intervalId = setInterval(updateState, 1000); // Mise à jour toutes les 1000 millisecondes (1 seconde)

        // Nettoyer l'intervalle quand le composant est démonté
        return () => clearInterval(intervalId);
    }, []); // Le tableau vide signifie que cet effet ne dépend d'aucune variable d'état et ne s'exécute qu'au montage

    return (
        <div className={"app-header"}>
            <p>{name}</p>
            <div className={"app-header-money"}>
                <p>{money}</p>
                <img src={"/img/coin.png"} alt={"Coin"}/>
            </div>
        </div>
    );
};

export default Header;