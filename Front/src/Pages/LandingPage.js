import "./css/LandingPage.css";
import React, {useEffect} from "react";
import {useNavigate} from "react-router-dom";

const LandingPage = () => {
    const navigate = useNavigate()
    const onNavigate = (link) => {
        navigate(link)
    };

    return (
        <div className={"landing-page"}>
            <div className={"menu"}>
                <div className={"option-col"}>
                    <div className={"option-row"}>
                        <div className={"option type-fire"}>
                            <h1 className={"option-title"}>INVENTAIRE</h1>
                            <img className={"option-image"} src={"/img/bag.png"} alt={"bag"}></img>
                        </div>
                        <div className={"option type-grass"} onClick={() => onNavigate("/shop")}>
                            <h1 className={"option-title"}>BOUTIQUE</h1>
                            <img className={"option-image"} src={"/img/dollar.png"} alt={"boutique"}></img>
                        </div>
                    </div>
                </div>
                <div className={"option-col"}>
                    <div className={"option-row"}>
                        <div className={"option type-water"}>
                            <h1 className={"option-title"} onClick={() => onNavigate("/eggs")}>OEUFS</h1>
                            <img className={"option-image"} src={"/img/egg.png"} alt={"egg"}></img>
                        </div>
                        <div className={"option type-electric"}>
                        <h1 className={"option-title"} onClick={() => onNavigate("/monsters")}>MONSTRES</h1>
                            <img className={"option-image"} src={"/img/psyduck_512.png"} alt={"monstre"}></img>
                        </div>
                    </div>
                </div>
                <div className={"option-col"}>
                    <div className={"option-row"}>
                        <div className={"option type-fighting"} onClick={() => onNavigate("/player")}>
                            <h1 className={"option-title"}>JOUEUR</h1>
                            <img className={"option-image"} src={"/img/dresseur.png"} alt={"dresseur"}></img>
                        </div>
                        <div className={"option type-ice"}>
                        <h1 className={"option-title"}>TEST</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LandingPage;
