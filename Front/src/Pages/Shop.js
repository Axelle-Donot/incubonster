import React, { useEffect, useState } from "react"
import "./css/Shop.css"
import Article from "./Article"
import "./../services/shop.service"
import { updateEggs, updateMoney, getEggs } from "../services/shop.service"
import { addEggs } from "../services/player.service"

const Shop = () => {
  const [enoughMoney, setEnoughMoney] = useState(true)
  const [articles, setArticles] = useState([])

  // Initialiser `balance` avec la valeur de `sessionStorage` ou 100 par défaut si aucune valeur n'est trouvée
  const [balance, setBalance] = useState(() => {
    const savedBalance = sessionStorage.getItem("money")
    return savedBalance !== null ? parseFloat(savedBalance) : 100
  })

  useEffect(() => {
    const fetchData = async () => {
      const player_id = +sessionStorage.getItem("id")
      const response = await getEggs(player_id)
      if (response.status === 200) {
        setArticles(Array.isArray(response.data) ? response.data : [])
      }
    }

    fetchData()
  }, [])

  const handleBuy = async (price, egg_id) => {
    if (balance >= price) {
      const player_id = +sessionStorage.getItem("player")
      const newBalance = balance - price
      setBalance(newBalance) // Mettre à jour le solde
      await updateMoney(player_id)
      await updateEggs(egg_id, player_id)
      await addEggs(player_id)
      sessionStorage.setItem("money", newBalance.toString()) // Mettre à jour `sessionStorage`
      setArticles(articles.filter((egg) => egg.id !== egg.id))
    } else {
      setEnoughMoney(false)
    }
  }

  return (
    <>
      <div className={"shop-background"}></div>
      <div className={"shop-background2"}></div>
      <div className={"pokemon-articles"}>
        <div className={"scroll-articles"}>
          {articles &&
            articles.map((egg) => (
              <Article
                key={egg.id}
                id={egg.id}
                price={egg.price}
                onBuy={() => handleBuy(egg.price, egg.id)}
                enoughMoney={sessionStorage.getItem("money") >= egg.price}
              />
            ))}
        </div>
      </div>
    </>
  )
}

export default Shop
