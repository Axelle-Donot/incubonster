import axios from 'axios';

const url = "http://localhost:4005/shop";

const headers = (id) => {
    return {headers : {'Content-Type': 'application/json','player_id': id}}
}

export const getEggs = async (id) => {
    return await axios.get(`${url}/oeuf`,headers(id));
};

export const updateEggs = async(egg_id, id) => {
    const body = {
        egg_id:egg_id,
        is_buy:true
    }
    return await axios.put(`${url}/oeuf`,body,headers(id));
};

export const getMoney = async(id) => {
    return await axios.get(`${url}/oeuf`,headers(id));
};

export const updateMoney = async(money,id) => {
    const body = {
        money:money
    }
    return await axios.put(`${url}/oeuf`,body,headers(id));
};