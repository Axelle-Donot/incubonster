import axios from "axios";
const url = "http://localhost:4003/monster";

const headers = (id) => {
    return {headers : {'Content-Type': 'application/json','player_id': id}}
}
const addToQueue = async (id) => {
    return await axios.get(`${url}/addToQ`,headers(id))
}