import axios from "axios"

const url = "http://localhost:4004/player"

const headers = (id) => {
  return { headers: { "Content-Type": "application/json", player_id: id } }
}

export const login = async (username) => {
  const data = {
    pseudo: username,
  }
  return await axios.post(`${url}/login`, data)
}

export const updateMoney = async (newMoney, id) => {
  const data = {
    money: newMoney,
  }
  return await axios.put(`${url}/money/`, data, headers(id))
}

export const getMoney = async (id) => {
  return await axios.get(`${url}/money/`, headers(id))
}

export const getPlayerEggs = async (id) => {
  return await axios.get(`${url}/oeuf/`, headers(id))
}

export const addEggs = async (id) => {
  return await axios.post(`${url}/oeuf/`, {}, headers(id))
}

export const addMonster = async (id) => {
  return await axios.post(`${url}/add/monster`, {}, headers(id))
}

export const freeMonster = async (id) => {
  return await axios.post(`${url}/free/monster`, {}, headers(id))
}

export const canAddMonster = async (id) => {
  return await axios.get(`${url}/canAdd/monster`, headers(id))
}

export const getTeam = async (id) => {
  return await axios.get(`${url}/team`, headers(id))
}
