import axios from "axios";
const url = "http://localhost:4002/incubator";

const headers = (id) => {
    return {headers : {'Content-Type': 'application/json','player_id': id}}
}
export const canAdd = async(id) => {
    return await axios.get(`${url}/canAdd`,headers(id))
}

export const addIncubator = async(id) => {
    return await axios.post(`${url}/add/`,{},headers(id))
}

export const startCheckIncubation = async(id) => {
    return await axios.get(`${url}/checkIncubation/start`,headers(id));
}

export const stopCheckIncubation = async(id) => {
    return await axios.get(`${url}/checkIncubation/start`,headers(id));
}

export const getAllEggs = async(id) => {
    return await axios.get(`${url}/oeuf`,headers(id));
};

export const addEggs = async(timeOfIncubation, id) => {
    const data = {
        timeOfIncubation : timeOfIncubation
    }
    return await axios.post(`${url}/oeuf`,data,headers(id));
};