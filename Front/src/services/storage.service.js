import axios from 'axios';

const url = "http://localhost:4006/stockage";
const headers = (id) => {
    return {headers : {'Content-Type': 'application/json','player_id': id}}
}

const getAllMonsters = async (id) => {
    return await axios.get(`${url}/all`,headers(id))
}

const addMonsterInStorage = async (monster,id) => {
    return await axios.post(`${url}/add/monster`,monster,headers(id))
}